package com.example.mobileverification;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        TextView textView;

        Intent intent = getIntent();
        String phoneNumber = intent.getExtras().getString("phone");

        textView = (TextView) findViewById(R.id.myTextView);

        textView.setText("This is the authenticated phone number " + phoneNumber);
    }
}
